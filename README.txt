Versions:
  Tkinter -> No
  Curses  -> NO

Instructions:
  python3 -tkinter:
    Click the buttons to change the display text box
    Click enter to evaluate the expression

  pytho3 -curses:
    Select buttons with the *arrow keys*
    Press *Enter* to *press* a button

Extra Credit:
  Both Tkinter and Curses: 20 Points
  
  Cool feature XYZ:
    Is cool because the calculator keeps track of extra state
    5 Points

  Cool feature ABC:
    Is Really Super Cool because the calculator Accepts voice commands
    10 Points
  
